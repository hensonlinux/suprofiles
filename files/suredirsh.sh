#!/usr/bin/env sh

shopt -q login_shell && loginshell="-l"
export SHELL="/bin/bash"
exec $SHELL $loginshell
