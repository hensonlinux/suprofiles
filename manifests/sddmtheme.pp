class suprofiles::sddmtheme {
  vcsrepo { '/usr/share/sddm/themes/abstractdark-theme':
    ensure => present,
    provider => git,
    source => 'https://github.com/3ximus/abstractdark-sddm-theme.git',
    user => 'root',
  } ->
  file { '/etc/sddm.conf.d':
    ensure => 'directory',
    owner => 'root',
    group => 'root',
    mode => '0744',
  } ->
  file { '/etc/sddm.conf':
    ensure => 'present',
    owner => 'root',
    group => 'root',
    mode => '0644',
    require => File['/etc/sddm.conf.d'],
    source => 'puppet:///modules/suprofiles/sddm/sddm.conf',
  }
}
