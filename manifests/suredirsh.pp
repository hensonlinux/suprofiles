class suprofiles::suredirsh {
  file { '/usr/local/bin/suredirsh':
    ensure => 'present',
    owner => 'root',
    group => 'root',
    source => 'puppet:///modules/suprofiles/suredirsh.sh',
    mode => '0755',
  }
}
